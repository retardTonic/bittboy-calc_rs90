.PHONY: retrofw miyoo rs90 clean pc
retrofw:
	mipsel-linux-gcc main.c -o calc -ggdb -lSDL -lSDL_ttf -lm
	cp calc opkg/
	mksquashfs opkg calc.opk -noappend -no-xattrs
miyoo:
	arm-linux-gcc main.c -o calc -ggdb -lSDL -lSDL_ttf -lm
rs90:
	mipsel-linux-gcc main.c -o calc -DRS90 -ggdb -lSDL -lSDL_ttf -lm
	cp calc opkg/
	mksquashfs opkg calc.opk -noappend -no-xattrs
pc:
	gcc main.c -o calc -lSDL -lSDL_ttf -lm
clean:
	rm -rf calc

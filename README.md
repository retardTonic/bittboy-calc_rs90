## [Calculator for Bittboy (and other handhelds)](https://github.com/szymor/bittboy-calc) (RS-90 port)

### Description
A simple calculator made for Bittboy in mind. It supports elementary arithmetic operations, nothing fancy.

### How to build
##### Linux PC
Type `make pc`.
##### Bittboy/PocketGo/PowKiddy Q90/PowKiddy V90
Type `make miyoo`.
##### RetroFW-based consoles (RS-97/LDK/LDK Landscape)
Type `make retrofw`.
##### RetroMini/RS-90
Type `make rs90`.
